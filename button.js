


let navItems = document.querySelector("#navSession")
let profile = document.querySelector("#profileBtn")
let navRegister = document.querySelector("#registerBtn")
console.log(navItems)

let userToken = localStorage.getItem("token")
console.log(userToken)

if(!userToken) {
	navItems.innerHTML = 
	`
		<li class="nav-item"> 
			<a href="./pages/login.html" class="nav-link"> Log in </a>
		</li>		
	`
	navRegister.innerHTML = 
	`
		<li class="nav-item"> 
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>

	`
}

else {
	profile.innerHTML = 
	`
		<li class="nav-item"> 
			<a href="./pages/userProfile.html" class="nav-link" id="profileNav"> Profile </a>
		</li>


	`
	navItems.innerHTML =
	`
		
		<li class="nav-item"> 
			<a href="./pages/logout.html" class="nav-link"> Log out </a>
		</li>

	`

}