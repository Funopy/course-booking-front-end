	// getting id from url
	let params = new URLSearchParams(window.location.search) // isesearch niya yung URL 
	let courseId = params.get('courseId')
	let token = localStorage.getItem('token') 

	fetch(`https://obscure-shelf-70514.herokuapp.com/api/courses/activate/${courseId}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//redirect to course
				alert('The Course is now Active!')
				window.location.replace("./courses.html")
			} else {
				//redirect in creating course
				alert("Something went wrong")
			}
		})


console.log('deleting')


