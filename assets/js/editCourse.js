let editCourseForm = document.querySelector("#editCourse");


editCourseForm.addEventListener ('submit', (e) => {
	e.preventDefault()

	let courseName = document.querySelector('#courseName').value;
	let description = document.querySelector('#courseDescription').value
	let price = document.querySelector('#coursePrice').value	

	let params = new URLSearchParams(window.location.search) // isesearch niya yung URL 
	let courseId = params.get('courseId')
	let token = localStorage.getItem('token') 

	fetch('https://obscure-shelf-70514.herokuapp.com/api/courses', {
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId: `${courseId}`,
			name: courseName,
			description : description,
			price: price
			})
	})
		.then(res => {
		return res.json()
		})
		.then(data => {
			//create of new course sucessful
			if(data === true){
			//redirect to course page
			alert("The course is now edited!")
			window.location.replace("./courses.html")
		}else {
			// redirect in creating courses
			alert("Something went wrong.")
			}

		})
	})
