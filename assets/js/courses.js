let adminUser = localStorage.getItem("isAdmin");


// card footer will be dynamically rendered if the user is an admin or not 
let cardFooter;


if( adminUser == "true"){

fetch('https://obscure-shelf-70514.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {
	//log the data to check if you werre ablt to fetch the data from the server 
	console.log(data)

	//courseData will store the data to be rendered
	let courseData

	if(data.length < 1) {
		courseData = "No courses available"
	} else {
		//else iterate the courses colelction and display each courses 
		courseData = data.map(course => {
			//check the make up of each element inside the course collection
			console.log(course._id)

			//if the user is regular user, dispaly when the course was created 
			if(adminUser == "true" || adminUser) {
				//for admin user
			
				console.log(course.isActive)
				if (course.isActive == true) {
					cardFooter = 
				`
					<a href ="./editCourse.html?courseId=${course._id}" value="${course._id}" id= "editButton" class="btn btn-primary text-white btn-block editButton" >Edit</a>
					<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" id= "deleteButton" class="btn btn-primary text-white btn-block dangerButton">Delete</a>
					<a href="./enrollees.html?courseId=${course._id}" value="${course._id}" id= "listOfEnrollees" class="btn btn-primary text-white btn-block dangerButton">List of Enrollees</a>
					
				`
				}else {
						cardFooter = 
				`
					<a href="./activate.html?courseId=${course._id}" value="${course._id}" id= "reactivateButton" class="btn btn-primary text-white btn-block dangerButton">Reactivate</a>
					<a href="./enrollees.html?courseId=${course._id}" value="${course._id}" id= "listOfEnrollees" class="btn btn-primary text-white btn-block dangerButton">List of Enrollees</a>
					
				`
				}

			}
			return(
					`
						<div class="col-md-6 my-3">
							<div class= "card">
								<div class= "card-body">
									<h5 class= "card-title">Course name: ${course.name}</h5>
									<p class= "card-text text-left">Description: ${course.description}</p>
									<p class= "card-text text-left">Status: ${course.isActive}</p>
									<p class="card-text text-right">Price:   ₱ ${course.price}</p>
								</div>
								<div class= "card-footer">
									${cardFooter}
								</div>
							</div>
						</div>

					`

					


				)
		}).join("");    // dahil gumamit ng array
	}
	let container = document.querySelector("#coursesContainer")

	// get the value of courseData and assign it as the #courseContainer's content
	container.innerHTML = courseData;
})

// add model - if user is an admin, there will be a button to add a course

let modelButton =document.querySelector("#adminButton")

if (adminUser == "false" || !adminUser) {
	//if user is regular user, do not show add course button 
	modelButton.innerHTML = null
} else {
	modelButton.innerHTML = 
	`
		<div id= "addCourseButton" class= "col-md-2 offset-md-10">
			<a href= "./addCourse" class="btn-block btn-primary">Add Course</a>	
		</div>
	`
		let addCourseButton = document.querySelector('#addCourseButton')
		addCourseButton.addEventListener('click', (e) =>{
			e.preventDefault()
			window.location.replace("./addCourse.html")
		})
		
}

}else {
	fetch('https://obscure-shelf-70514.herokuapp.com/api/courses/active')
	.then(res => res.json())
	.then(data => {
	//log the data to check if you werre ablt to fetch the data from the server 
	console.log(data)

	//courseData will store the data to be rendered
	let courseData

	if(data.length < 1) {
		courseData = "No courses available"
	} else {
		//else iterate the courses colelction and display each courses 
		courseData = data.map(course => {
			//check the make up of each element inside the course collection
			console.log(course._id)

			//if the user is regular user, dispaly when the course was created 
			if(adminUser == "false" || !adminUser) {
				cardFooter=
				`
					<a href="./course.html?courseId=${course._id}" value= "${course._id}" class="btn btn-primary text-white btn-block editButton">
					Select Course
					</a>
				`
			}
			return(
					`
						<div class="col-md-6 my-3">
							<div class= "card">
								<div class= "card-body">
									<h5 class= "card-title">${course.name}</h5>
									<p class= "card-text text-left">${course.description}</p>
									<p class="card-text text-right"> ₱${course.price}</p>
								</div>
								<div class= "card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`


				)
		}).join("");    // dahil gumamit ng array
	}
	let container = document.querySelector("#coursesContainer")

	// get the value of courseData and assign it as the #courseContainer's content
	container.innerHTML = courseData;
})

// add model - if user is an admin, there will be a button to add a course

let modelButton =document.querySelector("#adminButton")

if (adminUser == "false" || !adminUser) {
	//if user is regular user, do not show add course button 
	modelButton.innerHTML = null
} 
		
}
