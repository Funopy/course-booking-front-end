//Add the logic that will show the login button when a user is not logged in


let navItems = document.querySelector("#navSession")
let profile = document.querySelector("#profileBtn")
let navRegister = document.querySelector("#registerBtn")
console.log(navItems)

let userToken = localStorage.getItem("token")
console.log(userToken)

if(!userToken) {
	navItems.innerHTML = 
	`
		<li class="nav-item"> 
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>		
	`
	navRegister.innerHTML = 
	`
		<li class="nav-item"> 
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

	`
}

else {
	profile.innerHTML = 
	`
		<li class="nav-item"> 
			<a href="./userProfile.html" class="nav-link" id="profileNav"> Profile </a>
		</li>


	`
	navItems.innerHTML =
	`
		
		<li class="nav-item"> 
			<a href="./logout.html" class="nav-link"> Log out </a>
		</li>

	`

}
