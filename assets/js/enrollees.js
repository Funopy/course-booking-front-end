let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token') 


fetch(`https://obscure-shelf-70514.herokuapp.com/api/courses/${courseId}`,{
	method:'GET',
	headers:{
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}

})

.then(res => res.json())
.then(data => {
	//console.log (data)
	//console.log (data.enrollees)
	
	let enrolleeIds = data.enrollees.map((enrollee)=>{
		return enrollee.userId
	})

	for (let enrolleeId of enrolleeIds){
		fetch(`https://obscure-shelf-70514.herokuapp.com/api/users/${enrolleeId}`,{
			method:'GET',
		headers:{
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`	
		}
	})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			
			let newLi= document.createElement('li');
			newLi.append(`${data.firstName} ${data.lastName}`)
			userEnrollees.append(newLi)
		})

	}

})

