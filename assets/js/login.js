let loginForm = document.querySelector('#logInUser');

//add an event listener to the form
//(e) meaning when it is triggered, it will create an event object
	loginForm.addEventListener('submit', (e) =>{
		e.preventDefault()
		let email = document.querySelector('#userEmail').value;
		let password = document.querySelector('#password').value;
		console.log(email);
		console.log(password);

		if (email == "" || password == ""){
			alert('Please input your email or password')
		}else{
			fetch('https://obscure-shelf-70514.herokuapp.com/api/users/login',{
				method: 'POST',
				headers: {
				'Content-Type': 'application/json'
		},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res =>{
			return res.json()
		})
		.then(data =>{
			if(data.accessToken){
		//store HWT in local storage
		//localstorgae nasa machine na natin yan, hindi na kikita
			localStorage.setItem('token',data.accessToken);
			//send fetch request to decode JWT and obtain user ID and role for storing in context
			fetch('https://obscure-shelf-70514.herokuapp.com/api/users/details',{
				headers:{
				Authorization: `Bearer ${data.accessToken}`
				}
			})
			.then(res =>{
				return res.json()
			})
			.then(data =>{
			//set the global user to have properties containing authinticated users id and role
				localStorage.setItem("id", data._id)
				localStorage.setItem("isAdmin", data.isAdmin)
				window.location.replace("./courses.html")
			})
		}else{
		//authentication failure
		alert("Email Address not found please register first")
		window.location.replace("./register.html")
		}
	})
	}

});